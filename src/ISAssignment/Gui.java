
// author: Timothy Quill 100997474
// date: 20/09/1986

package ISAssignment;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gui extends JFrame
{
	public JFrame frame;
	
	// button labels
	private JButton btnScenario1;
	private JButton btnScenario2;
	private JButton btnQuit;
	private JButton btnRestart;
	
	// image labels
	private JLabel lblHeating = new JLabel();
	private JLabel lblFridge = new JLabel();
	private JLabel lblLighting = new JLabel();
	private JLabel lblTv = new JLabel();
	private JLabel lblCar = new JLabel();
	private JLabel lblFixedPriceRetailer = new JLabel();
	private JLabel lblInDemandRetailer = new JLabel();
	private JLabel lblBulkRetailer = new JLabel();
	private JLabel lblLimitedRetailer = new JLabel();
	private JLabel lblBattery = new JLabel();
	private JLabel lblHome = new JLabel();
	
	// text labels
	private JLabel lblHeatingText = new JLabel();
	private JLabel lblFridgeText = new JLabel();
	private JLabel lblLightingText = new JLabel();
	private JLabel lblTvText = new JLabel();
	private JLabel lblCarText = new JLabel();
	private JLabel lblFixedPriceRetailerText = new JLabel();
	private JLabel lblInDemandRetailerText = new JLabel();
	private JLabel lblBulkRetailerText = new JLabel();
	private JLabel lblLimitedRetailerText = new JLabel();
	private JLabel lblBatteryText = new JLabel();
	private JLabel lblTimeText = new JLabel();
	private JLabel lblDateText = new JLabel();
	private JLabel lblBoughtText = new JLabel();
	private JLabel lblSoldText = new JLabel();
	private JLabel lblUsedText = new JLabel();
	private JLabel lblSavedText = new JLabel();
	
	// text
	private String boughtText;
	private String soldText;
	private String usedText;
	private String savedText;
	private String timeText;
	private String dateText;
	private String heatingText;
	private String fridgeText;
	private String lightingText;
	private String tvText;
	private String carText;
	private String fixedPriceRetailerText;
	private String inDemandRetailerText;
	private String bulkRetailerText;
	private String limitedRetailerText;
	private String batteryText;
	
	private double bought;
	private double sold;
	private double used;
	private double saved;
	
	//time fields
	private int minutes;
	private int hours;
	
	// date fields
	private int day;
	private int month;
	private int year;
	
	// heating fields
	private double heatingCurrentUsage;
	private double heatingTurnOffTime;
	private double heatingExpectedUsage;
	
	// fridge fields
	private double fridgeCurrentUsage;
	private double fridgeTurnOffTime;
	private double fridgeExpectedUsage;
	
	// lighting fields
	private double lightingCurrentUsage;
	private double lightingTurnOffTime;
	private double lightingExpectedUsage;
	
	// tv fields
	private double tvCurrentUsage;
	private double tvTurnOffTime;
	private double tvExpectedUsage;
	
	// car fields
	private double carCurrentUsage;
	private double carTurnOffTime;
	private double carExpectedUsage;

	// FixedPriceRetailer fields
	private double fixedPriceRetailerSellingPrice;
	private double fixedPriceRetailerBuyingPrice;
	
	// InDemandRetailer fields
	private double inDemandRetailerSellingPrice;
	private double inDemandRetailerBuyingPrice;

	// BulkRetailer fields
	private double bulkRetailerSellingPrice;
	private double bulkRetailerBuyingPrice;
	
	// LimitedRetailer fields
	private double limitedRetailerSellingPrice;
	private double limitedRetailerBuyingPrice;
	
	// battery fields
	private double elecStored;
	private double storageLimit;
	private JScrollPane scrollBar;
	private JPanel batteryColor = new JPanel();
	
	// start program?
	String scenario = "none";

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		// create window
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		/**
		 *  handles all of the button functionality
		 */
		btnScenario1 = new JButton("Scenario 1");
		btnScenario1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				scenario = "scenario 1";
				
			}
		});
		btnScenario1.setBounds(6, 484, 89, 38);
		frame.getContentPane().add(btnScenario1);
		
		btnScenario2 = new JButton("Scenario 2");
		btnScenario2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				scenario = "scenario 2";
			}
		});
		btnScenario2.setBounds(96, 484, 89, 38);
		frame.getContentPane().add(btnScenario2);
		
		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnQuit.setBounds(280, 484, 89, 38);
		frame.getContentPane().add(btnQuit);
		
		btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRestart.setBounds(187, 484, 89, 38);
		frame.getContentPane().add(btnRestart);
		
		// set panel
		setPanel();
				
		//set time and date
		setTime();
		setDate();
		
		/**
		 *  handles all of the home functionality
		 */
		
		lblHome = new JLabel("");
		Image home = new ImageIcon(this.getClass().getResource("/home.png")).getImage();
		lblHome.setIcon(new ImageIcon(home));
		lblHome.setBounds(431, 157, 150, 157);
		frame.getContentPane().add(lblHome);
		
		/**
		 *  handles all of the heating functionality
		 */
		
		// sets the heating image
		lblHeating = new JLabel("");
		Image heating = new ImageIcon(this.getClass().getResource("/heating.png")).getImage();
		lblHeating.setIcon(new ImageIcon(heating));
		lblHeating.setBounds(22, 13, 97, 76);
		frame.getContentPane().add(lblHeating);
		
		setHeatingData();
		
		/**
		 *  handles all of the fridge functionality
		 */
		
		// sets the fridge image
		lblFridge = new JLabel("");
		Image fridge = new ImageIcon(this.getClass().getResource("/fridge.png")).getImage();
		lblFridge.setIcon(new ImageIcon(fridge));
		lblFridge.setBounds(22, 104, 97, 93);
		frame.getContentPane().add(lblFridge);
		
		setFridgeData();
		
		/**
		 *  handles all of the lighting functionality
		 */
		
		// sets the lighting image
		lblLighting = new JLabel("");
		Image lighting = new ImageIcon(this.getClass().getResource("/lighting.png")).getImage();
		lblLighting.setIcon(new ImageIcon(lighting));
		lblLighting.setBounds(22, 209, 97, 93);
		frame.getContentPane().add(lblLighting);
		
		setLightingData();
		
		/**
		 *  handles all of the tv functionality
		 */
		
		// sets the tv image
		lblTv = new JLabel("");
		Image tv = new ImageIcon(this.getClass().getResource("/tv.png")).getImage();
		lblTv.setIcon(new ImageIcon(tv));
		lblTv.setBounds(22, 327, 97, 68);
		frame.getContentPane().add(lblTv);
		
		setTvData();
		
		/**
		 *  handles all of the car functionality
		 */
		
		// sets the car image
		lblCar = new JLabel("");
		Image car = new ImageIcon(this.getClass().getResource("/car.png")).getImage();
		lblCar.setIcon(new ImageIcon(car));
		lblCar.setBounds(22, 418, 97, 48);
		frame.getContentPane().add(lblCar);
		
		setCarData();
		
		/**
		 *  handles all of the retailer functionality
		 */
		
		// sets the FixedPriceRetailer image
		lblFixedPriceRetailer = new JLabel("");
		Image fixedPriceRetailer = new ImageIcon(this.getClass().getResource("/fixed_price_retailer.png")).getImage();
		lblFixedPriceRetailer.setIcon(new ImageIcon(fixedPriceRetailer));
		lblFixedPriceRetailer.setBounds(885, 13, 97, 93);
		frame.getContentPane().add(lblFixedPriceRetailer);
		
		// sets the InDemandRetailer image
		lblInDemandRetailer = new JLabel("");
		Image inDemandRetailer = new ImageIcon(this.getClass().getResource("/in_demand_retailer.png")).getImage();
		lblInDemandRetailer.setIcon(new ImageIcon(inDemandRetailer));
		lblInDemandRetailer.setBounds(885, 104, 97, 93);
		frame.getContentPane().add(lblInDemandRetailer);
		
		// sets the BulkRetailer image
		lblBulkRetailer = new JLabel("");
		Image bulkRetailer = new ImageIcon(this.getClass().getResource("/bulk_retailer.png")).getImage();
		lblBulkRetailer.setIcon(new ImageIcon(bulkRetailer));
		lblBulkRetailer.setBounds(885, 195, 97, 93);
		frame.getContentPane().add(lblBulkRetailer);
		
		// sets the LimitedRetailer image
		lblLimitedRetailer = new JLabel("");
		Image limitedRetailer = new ImageIcon(this.getClass().getResource("/limited_retailer.png")).getImage();
		lblLimitedRetailer.setIcon(new ImageIcon(limitedRetailer));
		lblLimitedRetailer.setBounds(885, 281, 97, 93);
		frame.getContentPane().add(lblLimitedRetailer);
		
		// set retailer data
		setFixedPriceRetailerData();
		setInDemandRetailerData();
		setBulkRetailerData();
		setLimitedRetailerData();
		
		/**
		 *  handles all of the battery functionality
		 */
		
		// sets the battery image
		lblBattery = new JLabel("");
		Image battery = new ImageIcon(this.getClass().getResource("/battery.png")).getImage();
		lblBattery.setIcon(new ImageIcon(battery));
		lblBattery.setBounds(885, 399, 97, 68);
		frame.getContentPane().add(lblBattery);
		
		setBatteryData();
	}
	
	/**
	 *  handles all of the right-bottom panel presentation
	 */
	public void setPanel()
	{	
		// set bought electricity text
		boughtText = "Bought (kWs): " + bought;
		
		lblBoughtText.setText(" Bought (kWs): null");
		lblBoughtText.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		lblBoughtText.setForeground(Color.BLACK);
		lblBoughtText.setBounds(510, 445, 150, 17);
		frame.getContentPane().add(lblBoughtText);
		
		// set sold electricity text
		soldText = "    Sold (kWs): " + sold;
		
		lblSoldText.setText("     Sold (kWs): null");
		lblSoldText.setForeground(Color.BLACK);
		lblSoldText.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		lblSoldText.setBounds(510, 466, 148, 16);
		frame.getContentPane().add(lblSoldText);
		
		// set used electricity text
		usedText = "   Used (kWs): " + used;
		
		lblUsedText.setText(usedText);
		lblUsedText.setForeground(Color.BLACK);
		lblUsedText.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		lblUsedText.setBounds(512, 486, 148, 17);
		frame.getContentPane().add(lblUsedText);
		
		// set saved $ text
		savedText = "      Saved ($): " + saved;
		
		lblSavedText.setText("       Saved ($): null");
		lblSavedText.setForeground(Color.BLACK);
		lblSavedText.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		lblSavedText.setBounds(510, 505, 150, 17);
		frame.getContentPane().add(lblSavedText);
	}
	
	/**
	 *  handles all of the time presentation
	 */
	public void setTime()
	{
		
		// sets the time text
		timeText = "Time:" + hours + ":" + minutes;
		
		lblTimeText.setText(timeText);
		lblTimeText.setForeground(Color.BLACK);
		lblTimeText.setBounds(383, 484, 81, 16);
		frame.getContentPane().add(lblTimeText);
		revalidate();
		repaint();
	}
	
	/**
	 *  handles all of the date presentation
	 */
	public void setDate()
	{
		// sets the date text
		dateText = "Date:" + day + "." + month + "." + year;
		
		lblDateText.setText(dateText);
		lblDateText.setForeground(Color.BLACK);
		lblDateText.setBounds(383, 503, 115, 16);
		frame.getContentPane().add(lblDateText);
	}
	
	/**
	 *  handles all of the heating data presentation
	 */	
	public void setHeatingData()
	{
		// sets the heating text
		heatingText = "<html>   Current usage: " + heatingCurrentUsage + 
					 "<br>Exp. turn off time: " + heatingTurnOffTime +
					 "<br>        Exp. usage: " + heatingExpectedUsage;
				
		lblHeatingText.setText(heatingText);
		lblHeatingText.setForeground(Color.RED);
		lblHeatingText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the heating text in a scroll bar
		JPanel heatingSbPanel = new JPanel();
		scrollBar = new JScrollPane(heatingSbPanel,
					            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 13, 200, 55);
		scrollBar.setViewportView(lblHeatingText);
		frame.getContentPane().add(scrollBar);
	}

	/**
	 *  handles all of the fridge data presentation
	 */	
	public void setFridgeData()
	{
		// sets fridge text
		fridgeText = new String ("<html>   Current usage: " + fridgeCurrentUsage + 
						 				 "<br>Exp. turn off time: " + fridgeTurnOffTime +
						 				 "<br>        Exp. usage: " + fridgeExpectedUsage);
		lblFridgeText = new JLabel(fridgeText);
		lblFridgeText.setForeground(Color.RED);
		lblFridgeText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the fridge text in a scroll bar
		JPanel fridgeSbPanel = new JPanel();
		scrollBar = new JScrollPane(fridgeSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 120, 200, 55);
		scrollBar.setViewportView(lblFridgeText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the lighting data presentation
	 */	
	public void setLightingData()
	{
		// sets lighting text
		lightingText = new String ("<html>   Current usage: " + lightingCurrentUsage + 
						 "<br>Exp. turn off time: " + lightingTurnOffTime +
						 "<br>        Exp. usage: " + lightingExpectedUsage);
		lblLightingText = new JLabel(lightingText);
		lblLightingText.setForeground(Color.RED);
		lblLightingText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the lighting text in a scroll bar
		JPanel lightingSbPanel = new JPanel();
		scrollBar = new JScrollPane(lightingSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 230, 200, 55);
		scrollBar.setViewportView(lblLightingText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the lighting data presentation
	 */	
	public void setTvData()
	{
		//sets the tv text
		tvText = new String ("<html>   Current usage: " + tvCurrentUsage + 
						 	 "<br>Exp. turn off time: " + tvTurnOffTime +
						 	 "<br>        Exp. usage: " + tvExpectedUsage);
		lblTvText = new JLabel(tvText);
		lblTvText.setForeground(Color.RED);
		lblTvText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the tv text in a scroll bar
		JPanel tvSbPanel = new JPanel();
		scrollBar = new JScrollPane(tvSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 330, 200, 55);
		scrollBar.setViewportView(lblTvText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the lighting data presentation
	 */	
	public void setCarData()
	{
		// sets the car text
		carText = new String ("<html>   Current usage: " + carCurrentUsage + 
							  "<br>Exp. turn off time: " + carTurnOffTime +
							  "<br>        Exp. usage: " + carExpectedUsage);
		lblCarText = new JLabel(carText);
		lblCarText.setForeground(Color.RED);
		lblCarText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the car text in a scroll bar
		JPanel carSbPanel = new JPanel();
		scrollBar = new JScrollPane(carSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 410, 200, 55);
		scrollBar.setViewportView(lblCarText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the lblFixedPriceRetailer data presentation
	 */	
	public void setFixedPriceRetailerData()
	{
		//sets the FixedPriceRetailer text
		fixedPriceRetailerText = new String ("<html>Selling at $" + fixedPriceRetailerSellingPrice + " / kWs" +
											"<br>Buying at $" + fixedPriceRetailerBuyingPrice + " / kWs");
		lblFixedPriceRetailerText = new JLabel(fixedPriceRetailerText);
		lblFixedPriceRetailerText.setForeground(Color.RED);
		lblFixedPriceRetailerText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the FixedPriceRetailer text in a scroll bar
		JPanel fixedPriceRetailerSbPanel = new JPanel();
		scrollBar = new JScrollPane(fixedPriceRetailerSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 30, 200, 55);
		scrollBar.setViewportView(lblFixedPriceRetailerText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the FixedPriceRetailer data presentation
	 */	
	public void setInDemandRetailerData()
	{
		// sets the InDemandRetailer text
		inDemandRetailerText = new String ("<html>Selling at $" + inDemandRetailerSellingPrice +" / kWs" +
											"<br>Buying at $" + inDemandRetailerBuyingPrice + " / kWs");
		lblInDemandRetailerText = new JLabel(inDemandRetailerText);
		lblInDemandRetailerText.setForeground(Color.RED);
		lblInDemandRetailerText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the InDemandRetailerText text in a scrollbar
		JPanel inDemandRetailerSbPanel = new JPanel();
		scrollBar = new JScrollPane(inDemandRetailerSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 120, 200, 55);
		scrollBar.setViewportView(lblInDemandRetailerText);
		frame.getContentPane().add(scrollBar);
	}
	
	public void setBulkRetailerData()
	{
		// sets the BulkRetailer text
		bulkRetailerText = new String ("<html>Selling at $" + bulkRetailerSellingPrice +" / kWs" +
											"<br>Buying at $" + bulkRetailerBuyingPrice + " / kWs");
		lblBulkRetailerText = new JLabel(bulkRetailerText);
		lblBulkRetailerText.setForeground(Color.RED);
		lblBulkRetailerText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the BulkRetailer text in a scroll bar
		JPanel bulkRetailerSbPanel = new JPanel();
		scrollBar = new JScrollPane(bulkRetailerSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 210, 200, 55);
		scrollBar.setViewportView(lblBulkRetailerText);
		frame.getContentPane().add(scrollBar);
	}
	
	public void setLimitedRetailerData()
	{
		// sets the BulkRetailer text
		limitedRetailerText = new String ("<html>Selling at $" + limitedRetailerSellingPrice +" / kWs" +
											"<br>Buying at $" + limitedRetailerBuyingPrice + " / kWs");
		lblLimitedRetailerText = new JLabel(limitedRetailerText);
		lblLimitedRetailerText.setForeground(Color.RED);
		lblLimitedRetailerText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the BulkRetailer text in a scroll bar
		JPanel limitedRetailerSbPanel = new JPanel();
		scrollBar = new JScrollPane(limitedRetailerSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 210, 200, 55);
		scrollBar.setViewportView(lblLimitedRetailerText);
		frame.getContentPane().add(scrollBar);
	}
	
	public void setBatteryData()
	{
		// animates the battery color
		int storedPercent = animateBattery(batteryColor);
				
		//sets the battery text
		batteryText = new String ("<html>" + elecStored + " kWs stored of<br>a " + storageLimit + " kW capacity");
		
		batteryColor.setBounds(890, 405 + (58 - storedPercent), 81, storedPercent);
		frame.getContentPane().add(batteryColor);
		lblLimitedRetailer.setBounds(885, 287, 97, 93);
		
		frame.getContentPane().add(lblLimitedRetailer);
		
		JScrollPane scrollPane = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(673, 301, 200, 55);
		frame.getContentPane().add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(672, 407, 200, 55);
		frame.getContentPane().add(scrollPane_1);
		lblBatteryText = new JLabel(batteryText);
		scrollPane_1.setRowHeaderView(lblBatteryText);
		lblBatteryText.setForeground(Color.RED);
		lblBatteryText.setVerticalAlignment(SwingConstants.TOP);
	}
	
	public int animateBattery(JPanel batteryColor)
	{
		double boxPercent;
		int batteryPercent;
		
		boxPercent = (58.0/100);
		elecStored = ISAssignment.BatteryAgent.getPowerStored();
		storageLimit = ISAssignment.BatteryAgent.getPowerStorageLimit();
		batteryPercent = (int) Math.round(storageLimit / 100 * elecStored);
		setBatteryColor(batteryPercent, batteryColor);
		
		return (int) Math.round(boxPercent * batteryPercent);
	}
	
	public void setBatteryColor(int stored, JPanel batteryColor)
	{
		if (stored < 25)
		{
			batteryColor.setBackground(Color.RED);
		}
		else if (stored < 75)
		{
			batteryColor.setBackground(Color.YELLOW);
		}
		else
		{
			batteryColor.setBackground(Color.GREEN);
		}
	}
}

		
		

